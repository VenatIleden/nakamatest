﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UMainCanvas : Singleton<UMainCanvas>
{
    #region ATTRIBUTES

    public const string serverDevideID = "c9f3f6695bb994a332681c33da751bc2e60b3c12";

    [Header("SignIn")]

    public InputField signInNameInput;
    public InputField signInPassInput;
    
    [Header("LogIn")]

    public InputField logInNameInput;
    public InputField logInPassInput;
    public InputField levelInput;

    [Header("General attributes")]

    public GameObject friendNotificationPanel;

    public Color[] actionColors = new Color[3];

    public Button sendActionButton;
    public Button receiveActionButton;

    public InputField IPInput;
    
    public InputField currentMatchIDText;
    public InputField currentUserIDText;
    public InputField currentUsernameText;

    public Text debugText;

    int currentColor = 0;
    
    [Header("Social")]
    
    public GameObject singleFriendPanel;

    public Transform friendsContainerList;

    public InputField newFriendNameInput;

    #endregion

    #region METHODS

    void Awake()
    {
        string deviceid = SystemInfo.deviceUniqueIdentifier;

        IPInput.text = deviceid.Equals(serverDevideID) ? "127.0.0.1" : "95.61.208.237";
        string serverIP = deviceid.Equals(serverDevideID) ? "127.0.0.1" : "95.61.208.237";

        signInNameInput.onValueChanged.AddListener((async) => { CheckNameAvailability(signInNameInput.text); });

        // Subscribe to all desired events

        NakamaClientObject.Instance.OnSendDebugText += AddToDebugContent;
        NakamaClientObject.Instance.OnMatchmessageReceived += ManageMatchMessageReceived;
        NakamaClientObject.Instance.OnNewFriendRequest += EnableNewFriendNotificationPanel;

        NakamaClientObject.Instance.LaunchAccountsAdminClient(serverIP);
    }

    #region SOCIAL

    public void AddSingleFriendToList(string userName)
    {
        AddToDebugContent("Creating new friend panel in list");

        GameObject newFriendPanel = Instantiate(singleFriendPanel, singleFriendPanel.transform.parent);

        newFriendPanel.transform.Find("UserName").GetComponent<Text>().text = userName;

        Button fight = newFriendPanel.transform.Find("FightButton").GetComponent<Button>();
        fight.onClick.AddListener(() =>
        {
            AddToDebugContent("Fight against player " + userName);
            NakamaClientObject.Instance.SearchForMatchByName(userName);
        });

        Button remove = newFriendPanel.transform.Find("RemoveFriendButton").GetComponent<Button>();
        remove.onClick.AddListener(() =>
        {
            AddToDebugContent("Removing friend from list :: " + userName);
            NakamaClientObject.Instance.RemoveFriendByName(userName);
        });

        newFriendPanel.SetActive(true);
    }

    public void EnableNewFriendNotificationPanel(string content, string senderID)
    {
        GameObject newFriendNotificationPanel = Instantiate(friendNotificationPanel, friendNotificationPanel.transform.parent);
        newFriendNotificationPanel.SetActive(true);

        Text t = newFriendNotificationPanel.transform.Find("NotificationText").GetComponent<Text>();
        t.text = content;

        Button accept = newFriendNotificationPanel.transform.Find("AcceptFriendButton").GetComponent<Button>();
        accept.onClick.AddListener(() =>
        {
            NakamaClientObject.Instance.AddNewFriendByID(senderID);
            Destroy(newFriendNotificationPanel);
        });

        Button reject = newFriendNotificationPanel.transform.Find("RejectFriendButton").GetComponent<Button>();
        reject.onClick.AddListener(() =>
        {
            NakamaClientObject.Instance.RemoveFriendByID(senderID);
            Destroy(newFriendNotificationPanel);
        });
    }

    public void ClearFriendsList()
    {
        Transform[] allFriendPanels = friendsContainerList.GetComponentsInChildren<Transform>();
        for (int a = 0; a < allFriendPanels.Length; a++)
        {
            if (allFriendPanels[a].gameObject.activeInHierarchy && allFriendPanels[a] != friendsContainerList)
            {
                Destroy(allFriendPanels[a].gameObject);
            }
        }
    }

    #endregion

    public async void CheckNameAvailability(string newUserName)
    {
        bool nameAlreadyExists = await NakamaClientObject.Instance.CheckUserNameAvailable(newUserName);

        if (nameAlreadyExists)
        {
            signInNameInput.GetComponent<Image>().color = Color.red;
        }
        else
        {
            signInNameInput.GetComponent<Image>().color = Color.green;
        }
    }

    public IEnumerator BSignIn()
    {
        string name = signInNameInput.text;
        string password = signInPassInput.text;

        // Save only user name into a different table for fast checking
        NakamaClientObject.Instance.SaveNewUserName(name);

        // Save both user name and password to make them available for future logins
        NakamaClientObject.Instance.SaveNewUserProfile(name, password);

        // Create a new session with provided data to start working
        string customSessionID = string.Join("_", name, password);
        yield return NakamaClientObject.Instance.InitCustomClient(customSessionID, name);

        // Save all user data for the first time with custom session identifier
        NakamaClientObject.Instance.SaveNewUserData(name, password);
    }

    public IEnumerator BLogIn()
    {
        string name = logInNameInput.text;
        string password = logInPassInput.text;

        bool exists = NakamaClientObject.Instance.CheckUserProfileExists(name, password);

        // Search for given user and password in global table
        if (NakamaClientObject.Instance.CheckUserProfileExists(name, password))
        {
            // If keyToken exists, create custom session
            string customSessionID = string.Join("_", name, password);
            await NakamaClientObject.Instance.InitCustomClient(customSessionID, name);

            // Retrieve data from custom session if we need to do something
            //NakamaClientObject.UserData myUserData = await NakamaClientObject.Instance.GetCurrentUserData(name, password);
        }
        else
        {
            
        }
    }

    public void BAddNewFriend()
    {
        NakamaClientObject.Instance.AddNewFriendByName(newFriendNameInput.text);
    }
    
    public void BSearchMatchByLevel(bool higher)
    {
        int myLevel = int.Parse(levelInput.text.ToString());
        NakamaClientObject.Instance.SearchForRandomMatch(myLevel, higher);
    }
    
    public void BSendAction()
    {
        currentColor = (currentColor + 1) % actionColors.Length;

        Image i = sendActionButton.image;
        i.color = actionColors[currentColor];

        NakamaClientObject.Instance.SendMatchMessage();
    }

    public void BReceiveAction()
    {
        currentColor = (currentColor + 1) % 3;

        Image i = receiveActionButton.image;
        i.color = actionColors[currentColor];
    }

    void ManageMatchMessageReceived(string message)
    {
        //string content = System.Text.Encoding.UTF8.GetString(message.State);
        //switch (message.OpCode)
        //{
        //    case 101:
        //    SendToConsole("OpCode was 101");
        //    break;
        //    default:
        //    SendToConsole(string.Format("User {0} sent {1}", message.UserPresence.Username, content));
        //    Enqueue(() =>
        //    {
        //        //UMainCanvas.Instance.BReceiveAction();
        //    });
        //    break;
        //}
    }

    public void AddToDebugContent(string newContent)
    {
        debugText.text += string.Format("> {0}\n", newContent);
        Debug.Log(newContent);
    }

    #endregion
}