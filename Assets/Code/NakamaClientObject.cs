﻿using Nakama;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class NakamaClientObject : Singleton<NakamaClientObject>
{
    #region ATTRIBUTES

    public const string SERVER_DEVICE_ID = "c9f3f6695bb994a332681c33da751bc2e60b3c12";

    public const string SERVER_EXT_IP = "95.61.208.237";
    public const string SERVER_INT_IP = "127.0.0.1";

    // EVENTS

    //public delegate void SendDebugText(string text);
    //public SendDebugText OnSendDebugText;

    public Action<string> OnSendDebugText;

    //public delegate void NewFriendRequest(string content, string senderID);
    //public NewFriendRequest OnNewFriendRequest;

    public Action<string, string> OnNewFriendRequest;

    //public delegate void MatchmessageReceived(string message);
    //public MatchmessageReceived OnMatchmessageReceived;

    public Action<string> OnMatchmessageReceived;

    //public delegate void UserRefreshedInfo(string userID, string userName);
    //public UserRefreshedInfo OnUserRefreshedInfo;

    public Action<string, string> OnUserRefreshedInfo;


    // NAKAMA OBJECTS

    IClient _client;
    IApiAccount _account;
    ISession _session;
    ISocket _socket;
    IMatch _currentMatch;

    // OTHER ATTRIBUTES

    public struct UserName
    {
        public string userName;
    }

    public struct UserProfile
    {
        public string userName;
        public string userPass;
    }

    public struct UserData
    {
        public string userName;
        public string userPass;
        public int userLevel;
    }

    Queue<IEnumerator> _executionQueue;

    #endregion

    #region METHODS

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        _executionQueue = new Queue<IEnumerator>(64);
        
        string deviceid = SystemInfo.deviceUniqueIdentifier;
        
        string serverIP = deviceid.Equals(SERVER_DEVICE_ID) ? SERVER_INT_IP : SERVER_EXT_IP;

        StartCoroutine(LaunchAccountsAdminClient(serverIP));
    }

    public IEnumerator LaunchAccountsAdminClient(string ip)
    {
        _client = new Client("defaultkey", ip, 7350, false);

        yield return _session = (ISession)_client.AuthenticateCustomAsync("empty_session", "accounts_admin");
        
        SendToConsole("Authenticated ADMIN session: " + _session);
    }

    public IEnumerator InitCustomClient(string sessionID, string userName)
    {
        yield return _session = (ISession)_client.AuthenticateCustomAsync(sessionID, userName);

        SendToConsole("Authenticated USER session from InitCustomClient: " + _session);

        yield return RefreshUserInfo(sessionID, userName);
        yield return InitSocket();
        RefreshFriendsList();
    }

    /// <summary>
    /// Creates a new socket object and initializes all required callbacks
    /// </summary>
    async Task InitSocket()
    {
        _socket = _client.CreateWebSocket();

        // Callbacks

        _socket.OnConnect += async (sender, args) =>
        {
            SendToConsole("Socket connected");
            await _socket.UpdateStatusAsync("");
        };

        _socket.OnDisconnect += (sender, args) =>
        {
            SendToConsole("Socket disconnected");
        };

        _socket.OnMatchState += (_, state) =>
        {
            SendToConsole("Message received...");

            Enqueue(() =>
            {
                // Should check if it's possible to deserialize this message or we need to send the IMatchState object
                OnMatchmessageReceived?.Invoke(state.ToString());
            });
        };

        _socket.OnStatusPresence += (_, presence) =>
        {
            StatusPresenceUpdateManaging(presence);
        };

        _socket.OnMatchmakerMatched += async (_, matched) =>
        {
            SendToConsole(string.Format("Found random match: {0}", matched.Users));

            _currentMatch = await _socket.JoinMatchAsync(matched);
            SendToConsole("These players are already in the match:");
            foreach (var presence in _currentMatch.Presences)
            {
                SendToConsole(string.Format("<'{0}'>\t ID = '{1}'", presence.Username, presence.UserId));
            }
        };

        _socket.OnNotification += (_, notification) =>
        {
            SendToConsole(string.Format("Received notification {0}", notification));
            SendToConsole(string.Format("Notification content {0}", notification.Content));
            NotificationsReceivedManaging(notification);
        };

        // ...

        await _socket.ConnectAsync(_session);
    }

    async Task RefreshUserInfo(string sessionID, string userName)
    {
        await _client.UpdateAccountAsync(_session, userName, userName);

        if (_session.Username != userName)
        {
            SendToConsole("Previous session user name did not match current. Refreshing...");
            _session = await _client.AuthenticateCustomAsync(sessionID, userName);
        }

        _account = await _client.GetAccountAsync(_session);

        SendToConsole("Authenticated session from RefreshUserInfo: " + _session);

        Enqueue(() => OnUserRefreshedInfo?.Invoke(_account.User.Id, _account.User.Username));
    }

    public void SendMatchMessage()
    {
        SendToConsole("Message sent...");
        long opCode = 1;
        string content = JsonUtility.ToJson(new Dictionary<string, string> { { "hello", "world" } });
        _socket.SendMatchState(_currentMatch.Id, opCode, content);
    }

    #region DATA BASE

    #region WRITE

    public async void SaveNewUserName(string name)
    {
        SendToConsole("Preparing to save new user name...");

        // Generate a string with collection name where we want to save our data
        string collection = "registeredUsers";

        // Generate a unique key which only the real user will know
        string keyToken = name;

        UserName newUserName = new UserName
        {
            userName = name
        };

        // Create a JSON with the data
        string serializedUserData = JsonUtility.ToJson(newUserName);

        await SaveValueToDataBase(collection, keyToken, serializedUserData, 2, 1);
    }

    public async void SaveNewUserProfile(string name, string pass)
    {
        SendToConsole(string.Format("Registering new user profile: {0}", name));

        // Generate a string with collection name where we want to save our data
        string collection = "userProfiles";

        // Generate a unique key which only the real user will know
        string keyToken = string.Join("_", name, pass);

        // Create a new struct with the given data
        UserProfile newUser = new UserProfile
        {
            userName = name,
            userPass = pass
        };

        // Create a JSON with the data
        string serializedUserData = JsonUtility.ToJson(newUser);

        // Call generic method for data saving
        await SaveValueToDataBase(collection, keyToken, serializedUserData, 2, 1);
    }

    public async void SaveNewUserData(string name, string pass)
    {
        SendToConsole(string.Format("Saving full user data block after moving to custom session: {0}", name));

        // Generate a string with collection name where we want to save our data
        string collection = "allUserData";

        // Generate a unique key which only the real user will know
        string keyToken = string.Join("_", name, pass);

        // Create a new struct with the given data
        UserData newUser = new UserData
        {
            userName = name,
            userPass = pass,
            userLevel = 0
        };

        // Create a JSON with the data
        string serializedUserData = JsonUtility.ToJson(newUser);

        // Call generic method for data saving
        await SaveValueToDataBase(collection, keyToken, serializedUserData, 2, 1);
    }

    /// <summary>
    /// Generic method which saves any data in DB | 
    /// For more information refer to https://heroiclabs.com/docs/storage-collections/
    /// </summary>
    /// <param name="collection">"Table" where data value will be written</param>
    /// <param name="keyToken">Unique value which identifies this data</param>
    /// <param name="dataValue">Data we're about to save (must be JSON format)</param>
    /// <param name="permissionRead">"Public Read" (2), "Owner Read" (1), or "No Read" (0)</param>
    /// <param name="permissionWrite">"Owner Write" (1), or "No Write" (0)</param>
    public async Task SaveValueToDataBase(string collection, string keyToken, string dataValue, int permissionRead, int permissionWrite)
    {
        SendToConsole(string.Format("Creating new write storage object: \nCollection ->{0}\nKey ->{1}\nValue ->{2}", collection, keyToken, dataValue));

        // Create a Nakama formatted writing object and add our struct (note that collection and key are hardcoded values)
        WriteStorageObject writeObject = new WriteStorageObject
        {
            Collection = collection,
            Key = keyToken,
            Value = dataValue,
            PermissionRead = permissionRead,
            PermissionWrite = permissionWrite
        };

        try
        {
            // Ask for persistent record to the DB
            IApiStorageObjectAcks record = await _client.WriteStorageObjectsAsync(_session, writeObject);

            SendToConsole(string.Format("Successfully stored objects {0}", record));
        }
        catch (Exception e)
        {
            SendToConsole(string.Format("<!> Problem saving data: {0}", e.Message));
        }
    }

    #endregion

    #region READ

    public async Task<bool> CheckUserNameAvailable(string userName)
    {
        // Generate a string with collection name where we want to get data from
        string collection = "registeredUsers";

        // Generate a unique key which shall identify the register we're looking for
        string keyToken = userName;

        string rawData = await GetDataFromDB(collection, keyToken, _session.UserId);

        return !(string.IsNullOrEmpty(rawData));
    }

    public IEnumerator<bool> CheckUserProfileExists(string name, string pass)
    {
        bool userProfileExists = false;

        // Generate a string with collection name where we want to get data from
        string collection = "userProfiles";

        // Generate a unique key which only the real user will know
        string keyToken = string.Join("_", name, pass);

        string rawData = await GetDataFromDB(collection, keyToken, _session.UserId);

        if (!string.IsNullOrEmpty(rawData))
        {
            UserProfile retrievedUser = JsonUtility.FromJson<UserProfile>(rawData);

            SendToConsole(string.Format("Success: {0} | {1}", retrievedUser.userName, retrievedUser.userPass));

            userProfileExists = true;
        }
        else
        {
            SendToConsole("<!> ERROR, user does not exist or password is wrong");
        }

        return userProfileExists;
    }

    public async Task<UserData> GetCurrentUserData(string name, string pass)
    {
        UserData foundUser = new UserData();

        // Generate a string with collection name where we want to get data from
        string collection = "allUserData";

        // Generate a unique key which only the real user will know
        string keyToken = string.Join("_", name, pass);

        string rawData = await GetDataFromDB(collection, keyToken, _session.UserId);

        if (!string.IsNullOrEmpty(rawData))
        {
            foundUser = JsonUtility.FromJson<UserData>(rawData);

            SendToConsole(string.Format("Success: {0} | {1} | {2}", foundUser.userName, foundUser.userPass, foundUser.userLevel));
        }
        else
        {
            SendToConsole("<!> ERROR, could not find given user");
        }

        return foundUser;
    }

    /// <summary>
    /// Generic method which retrieves any data in DB | 
    /// For more information refer to https://heroiclabs.com/docs/storage-collections/
    /// </summary>
    /// <param name="collection">"Table" where data value will be written</param>
    /// <param name="keyToken">Unique value which identifies this data</param>
    /// <param name="userID">Identifier which decides whether we have permission or not for accessing the data</param>
    public IEnumerator<string> GetDataFromDB(string collection, string keyToken, string userID)
    {
        SendToConsole(string.Format("Creating new storage object for READING User data: \nCollection ->{0}\nKey ->{1}\nUserID ->{2}", collection, keyToken, userID));

        string retrievedData = "";
        
        try
        {
            StorageObjectId requestedRecord = new StorageObjectId
            {
                Collection = collection,
                Key = keyToken,
                UserId = userID
            };

            IApiStorageObjects result = (IApiStorageObjects)_client.ReadStorageObjectsAsync(_session, requestedRecord);

            // Deserialize data
            foreach (IApiStorageObject o in result.Objects)
            {
                retrievedData += o.Value;
            }
        }
        catch (Exception e)
        {
            SendToConsole(string.Format("<!> Problem getting data from DB: {0}", e.Message));
        }
        yield return retrievedData;
    }

    #endregion

    #endregion

    void StatusPresenceUpdateManaging(IStatusPresenceEvent presence)
    {
        foreach (IUserPresence join in presence.Joins)
        {
            SendToConsole(string.Format("Detected new presence with this data: '{0}' ", join.Status));

            // Know if the presense is only saying hello or publishing a match
            if (join.Status == "")
            {
                // If it's saying hello, start following for future updates
                _socket.FollowUsersAsync(new[] { join.UserId });
            }
        }
    }

    void NotificationsReceivedManaging(IApiNotification notification)
    {
        // Check notification code           
        switch (notification.Code)
        {
            // -1  User X wants to chat.
            case -1:
                {
                    break;
                }
            // -2  User X wants to add you as a friend.
            case -2:
                {
                    Enqueue(() =>
                    {
                        OnNewFriendRequest?.Invoke(notification.Subject, notification.SenderId);
                    });
                    break;
                }
            // -3  User X accepted your friend invite.
            case -3:
                {
                    // Refresh friends list
                    RefreshFriendsList();
                    break;
                }
            // -4  You've been accepted to X group.
            case -4:
                {
                    break;
                }
            // -5  User X wants to join your group.
            case -5:
                {
                    break;
                }
            // -6  Your friend X has just joined the game.
            case -6:
                {
                    break;
                }
                // It is possible to add new notification codes when creating them
        }
    }

    void OnApplicationQuit()
    {
        if (_socket != null)
        {
            _socket.DisconnectAsync(false);
        }
    }

    void SendToConsole(string message)
    {
        Enqueue(() =>
        {
            OnSendDebugText?.Invoke(message);
            Debug.Log(message);
        });
    }

    #region SOCIAL

    public async void AddNewFriendByName(string userName)
    {
        SendToConsole(string.Format("Pressed Add Friend with name :: {0}", userName));
        if (userName != "")
        {
            string[] names = new[] { userName };
            await _client.AddFriendsAsync(_session, null, names);
        }
    }

    public async void AddNewFriendByID(string userID)
    {
        SendToConsole(string.Format("Accepted Add Friend with id :: {0}", userID));
        string[] ids = new[] { userID };
        await _client.AddFriendsAsync(_session, ids, null);
        RefreshFriendsList();
    }

    public async void RemoveFriendByName(string userName)
    {
        string[] names = new[] { userName };
        await _client.DeleteFriendsAsync(_session, null, names);
        RefreshFriendsList();
    }

    public async void RemoveFriendByID(string userID)
    {
        string[] ids = new[] { userID };
        await _client.DeleteFriendsAsync(_session, ids, null);
        RefreshFriendsList();
    }

    async void RefreshFriendsList()
    {
        // Get all friends list
        IApiFriends allFriends = await _client.ListFriendsAsync(_session);

        // Clean list
        Enqueue(() =>
        {
            //UMainCanvas.Instance.ClearFriendsList();
        });

        foreach (IApiFriend singleFriend in allFriends.Friends)
        {
            // Check state for each one: 
            // 0   Users are friends with each other.
            // 1   User A has sent an invitation and pending acceptance from user B.
            // 2   User A has received an invitation but has not accepted yet.
            if (singleFriend.State == 0)
            {
                await _socket.FollowUsersAsync(new[] { singleFriend.User.Id });
                Enqueue(() =>
                {
                    //UMainCanvas.Instance.AddSingleFriendToList(singleFriend.User.Username);
                });
            }
        }
    }

    #endregion

    #region MATCH MAKING

    /// <summary>
    /// For more information about queries in Nakama refer to this site: http://blevesearch.com/docs/Query-String-Query/
    /// </summary>
    public async void SearchForMatchByName(string rivalName)
    {
        string query = string.Format("properties.name:{0} +properties.name:{1}", _account.User.Username, rivalName);
        int minCount = 2;
        int maxCount = 2;
        Dictionary<string, string> stringProperties = new Dictionary<string, string>() { { "name", _account.User.Username } };

        SendToConsole(string.Format("Searching for match by NAME with this query: '{0}'", query));

        IMatchmakerTicket matchmakerTicket = await _socket.AddMatchmakerAsync(query, minCount, maxCount, stringProperties);
    }

    /// <summary>
    /// For more information about queries in Nakama refer to this site: http://blevesearch.com/docs/Query-String-Query/
    /// </summary>
    public async void SearchForRandomMatch(int myLevel, bool higher = true)
    {
        string selfName = _account.User.Username;
        string levelComparisson = higher ? ">=" : "<";

        string query = string.Format("properties.level:{0}{1} -properties.name:{2}", levelComparisson, myLevel, selfName);
        int minCount = 2;
        int maxCount = 2;
        Dictionary<string, string> stringProperties = new Dictionary<string, string>() { { "name", selfName } };
        Dictionary<string, double> numericProperties = new Dictionary<string, double>() { { "level", myLevel } };

        SendToConsole(string.Format("Searching for match by LEVEL with this query: '{0}'", query));

        IMatchmakerTicket matchmakerTicket = await _socket.AddMatchmakerAsync(query, minCount, maxCount, stringProperties, numericProperties);
    }

    #endregion

    #region ACTIONS QUEUE SYSTEM

    /// <summary>
    /// Adds the given action to the queue ensuring no other method (Update) is accessing it
    /// </summary>
    /// <param name="action"></param>
    void Enqueue(Action action)
    {
        lock (_executionQueue)
        {
            _executionQueue.Enqueue(ActionWrapper(action));
        }
    }

    /// <summary>
    /// Simply executes any given action and returns null because it's mandatory for coroutines
    /// </summary>
    /// <param name="action">Lambda expression with one or more statements</param>
    /// <returns></returns>
    IEnumerator ActionWrapper(Action action)
    {
        action();
        yield return null;
    }

    /// <summary>
    /// Executes all actions enqueued during last frame and ensures none new is added from 'Enqueue'
    /// </summary>
    void Update()
    {
        lock (_executionQueue)
        {
            for (int i = 0; i < _executionQueue.Count; i++)
            {
                StartCoroutine(_executionQueue.Dequeue());
            }
        }
    }

    #endregion

    #endregion
}